background yes
update_interval 1

cpu_avg_samples 1
net_avg_samples 2
temperature_unit celsius

double_buffer yes
no_buffers yes
text_buffer_size 2048
override_utf8_locale yes
use_xft yes
xftfont caviar dreams:size=10
xftalpha 0.5
uppercase no

gap_x 20
gap_y 70
minimum_size 300 900
maximum_width 350

own_window yes
own_window_type normal
own_window_transparent yes
own_window_argb_visual yes
own_window_argb_visual no
own_window_colour 000000
own_window_argb_value 0
own_window_hints undecorate,sticky,skip_taskbar,skip_pager,below

border_inner_margin 0
border_outer_margin 0
alignment top_right

draw_shades no
draw_outline no
draw_borders no
draw_graph_borders no

TEXT

CALENDAR ${hr 2}
${font DejaVu Sans Mono:size=8}${execpi 3600 DJS=`date +%_d`; cal -m | sed '1d' | sed '/./!d' | sed 's/$/                     /' | fold -w 21 | sed -n '/^.\{21\}/p' | sed 's/^/${alignc} /' | sed /" $DJS "/s/" $DJS "/" "'${color2}'"$DJS"'${color}'" "/}${font}

SYSTEM ${hr 2}

${alignc 24}${font Arial Black:size=14}${nodename}${font}
${font OpenLogos:size=16}u${font} Kernel: ${alignr}${kernel}
${font StyleBats:size=16}A${font} CPU1: ${cpu cpu1}% ${alignr}${cpubar cpu1 8,60}
${font StyleBats:size=16}A${font} CPU2: ${cpu cpu2}% ${alignr}${cpubar cpu2 8,60}
${font StyleBats:size=16}h${font} GPU:${execi 60 aticonfig --od-getclocks | awk '/load/ {print $4}' | sed 's/%//'}%${alignr}${execibar 60 aticonfig --od-getclocks | awk '/load/ {print $4}'| sed 's/%//'}
${font StyleBats:size=16}g${font} RAM: $memperc% ${alignr}${membar 8,60}
${font StyleBats:size=16}q${font} Uptime: ${alignr}${uptime}

PROCESSES ${hr 2}

NAME ${goto 120}CPU%${goto 180}IO%${alignr}MEM%
${top name 1}${goto 120}${top cpu 1}${goto 170}${top io_perc 1}${alignr}${top mem 1}
${top name 2}${goto 120}${top cpu 2}${goto 170}${top io_perc 2}${alignr}${top mem 2}
${top name 3}${goto 120}${top cpu 3}${goto 170}${top io_perc 3}${alignr}${top mem 3}

DISK SPACE ${hr 2}

/${goto 45}${fs_bar 8,50 /}${goto 95} Used: ${fs_used /}${goto 185} Free: ${fs_free /}
/home ${goto 45}${fs_bar 8,50 /home}${goto 95} Used: ${fs_used /home}${goto 185} Free: ${fs_free /home}
/tmp ${goto 45}${fs_bar 8,50 /tmp}${goto 95} Used: ${fs_used /tmp}${goto 185} Free: ${fs_free /tmp}

FAN SPEEDS & TEMPERATURES ${hr 2}

CPU Fan Speed ${alignr}${exec sensors | grep fan1 | cut -d " " -f 9,10}
Fan Speed ${alignr}${exec sensors | grep fan2 | cut -d " " -f 9,10}
GPU Fan Speed ${alignr}${exec aticonfig --pplib-cmd "get fanspeed 0" | awk '/Result/ {print $4}'}
Thermistor ${alignr}${execi 10 sensors | grep temp1 | cut -d " " -f 9| grep -v "^$"}
Thermal diode ${alignr} ${execi 10 sensors | grep temp2 | cut -d " " -f 9| grep -v "^$"}
INTEL PECI ${alignr}${execi 10 sensors | grep temp3 | cut -d " " -f 9| grep -v "^$"}
AMD Radeon HD5750 ${alignr}${execi 60 aticonfig --od-gettemperature | grep Temperature | cut -d " " -f 23}°C
${execi 60 sudo hddtemp SATA:/dev/sda1 | awk '{print $2,$3}'} ${alignr}${execi 60 sudo hddtemp SATA:/dev/sda1 | awk '{print $4}'}
${execi 60 sudo hddtemp SATA:/dev/sdc1 | awk '{print $2,$3}'} ${alignr}${execi 60 sudo hddtemp SATA:/dev/sdc1 | awk '{print $4}'}
${execi 60 sudo hddtemp SATA:/dev/sdb1 | awk '{print $2,$3}'} ${alignr}${execi 60 sudo hddtemp SATA:/dev/sdb1 | awk '{print $4}'}

NETWORK ${hr 2}

${if_existing /proc/net/route eth0}
${voffset -6}${font PizzaDude Bullets:size=14}O${font} Up: ${upspeed eth0}${alignr}${upspeedgraph eth0 8,60 black black}
${voffset 4}${font PizzaDude Bullets:size=14}U${font} Down: ${downspeed eth0}${alignr}${downspeedgraph eth0 8,60 black black}
${voffset 4}${font PizzaDude Bullets:size=14}N${font} Upload: ${alignr}${totalup eth0}
${voffset 4}${font PizzaDude Bullets:size=14}T${font} Download: ${alignr}${totaldown eth0}
${endif}

PACMAN ${hr 2}

${execpi 1800 python2 /etc/conky/pacman.py}

CLEMENTINE ${hr 2}

${execp conkyClementine --template=/etc/conky/conkyclementine/conkyClementine.template}

WEATHER ${hr 2}

${goto 80}${font weather:size=46}${exec /etc/conky/google_weather/weather.sh "Dresden,Germany" cp}${font}
${exec /etc/conky/google_weather/weather.sh "Dresden,Germany"}

${alignc}${execi 60 /etc/conky/google_weather/weather.sh "Dresden,Germany" dl}
${font weather:size=46}${execi 60 /etc/conky/google_weather/weather.sh "Dresden,Germany" fcp}${font}
${alignc}${execi 60 /etc/conky/google_weather/weather.sh "Dresden,Germany" fct}

REDDIT ${hr 2}

${font}Messages: ${if_empty ${rss http://www.reddit.com/message/unread/.rss?feed=fb4759ead39540799fa7e2537320695c9cfe93f3&user=zufallsheld 1 item_titles 1}}${image /etc/conky/reddit/message.png -p 246,1274}${else}${image /etc/conky/reddit/newmessage.png -p 246,1274}${endif}

${font}Link Karma: ${alignr}${execpi 300 wget -O - http://www.reddit.com/user/zufallsheld/about.json | awk '{match($0, "k_karma\": ([0-9]+)", a); print a[1];}'}
${font}Comment Karma: ${alignr}${execpi 300 wget -O - http://www.reddit.com/user/zufallsheld/about.json | awk '{match($0, "t_karma\": ([0-9]+)", a); print a[1];}'}

