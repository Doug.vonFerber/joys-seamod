#!/usr/bin/perl
use strict;
use LWP::Simple;
use Cache::FileCache;

#use Finance::Quote;
#my $q = Finance::Quote->new;
#my $conversion_rate = $q->currency("USD","CAD");

my %patterns =
(
 'last'                => 'id="ref_\d+_l">([\d\.]+)</span><br>',
 'open'                => 'id="ref_\d+_op">([\d\.]+)</span></td>',
 'high'                => 'id="ref_\d+_hi">([\d\.]+)</span></td>',
 'low'                 => 'id="ref_\d+_lo">([\d\.]+)</span></td>',
 'volume'              => 'id="ref_\d+_vo">([\d\.]+M?)</span></td>',
 'todays_change'       => 'id="ref_\d+_c">([-+]?[\d\.]+)</span></span>',
);
my ( undef, undef, $hour, undef, undef, undef, $wday ) = gmtime();
my ( $market, $symbol, $quantity ) = @ARGV;

my $cache = new Cache::FileCache();
my $cache_time = (( ( $wday > 0 ) && ( $wday < 6 ) && ( $hour < 9 ) ) ? ( '10 seconds' ) : ( '1 hour' ));
my $cache_name = ( ".google_finance-$market-$symbol.cache" );
my $html = $cache->get( $cache_name );
if ( not defined $html )
  {
  $html = get( 'http://finance.google.ca/finance?client=ob&q='.$market.':'.$symbol );
  $cache->set( $cache_name, $html, $cache_time );
  }

my %data;
foreach my $key ( keys( %patterns ) )
  {
  $html =~ m"$patterns{$key}" || next;
  $data{$key} = $1;
  }

my $color = (( $data{'todays_change'} >= 0 ) ? ( 'green' ) : ( 'red' ));
my $value = sprintf("%0.2f", ( $data{'last'} * $quantity ));
my $percent = sprintf("%0.2f%%",(($data{'todays_change'} / $data{'open'}) * 100));

printf (
        '${color white} %-7s%-6s${color %s}%-5s(%s)${color white}%8s%6s%12s'."\n",
        ( $market, $symbol, $color, $data{'todays_change'}, $percent, $data{'last'}, $quantity, $value )
       );
